# Xtramile Solution Recruitment Test 2 Repository

This repository contains the source code for the recruitment test 2 of Xtramile Solution.

## Solution Structure

The solution is organized into three projects:

1. **Test2API**: This project contains the source code for the API.
2. **Test2WEB**: This project contains the source code for the user interface (UI).
3. **Test2Test**: This project contains unit tests implemented using xUnit.

## Getting Started

To run the solution in Visual Studio, follow these steps:

1. Open the solution in Visual Studio.

2. Right-click on the solution in the Solution Explorer and select "Properties."

3. In the Solution Properties window, go to the "Common Properties" section and select "Startup Project."

4. Choose "Multiple Startup Projects."

5. For both `Test2API` and `Test2WEB`, set the "Action" to "Start."

6. Click "OK" to save the changes.

7. Run the solution, and both projects will start simultaneously.

## Running the Projects

- **Test2API** will be accessible at [http://localhost:5000](http://localhost:5000).

- **Test2WEB** will be accessible at [http://localhost:5001](http://localhost:5001).

## OpenWeatherMap API Key

If needed, you can change the OpenWeatherMap API key used by the application. To do this, navigate to the `Test2API` project and locate the `WeatherService` class. Inside this class, there is a variable named `apiKey`, which you can modify with your own API key.

```csharp
private const string apiKey = "YOUR_API_KEY_HERE";
