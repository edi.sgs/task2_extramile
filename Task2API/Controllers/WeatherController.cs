﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task2API.Model;
using Task2API.Services;

namespace Task2API.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]

    public class WeatherController : ControllerBase
    {
        private readonly IWeatherService _weatherService;
        public WeatherController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        // Endpoint : /weather/data?query=QUERY
        // QUERY means City name
        [HttpGet]
        public async Task<WeatherModel> Data(string query)
        {
            return await _weatherService.GetWeatherAsync(query);
        }
    }
}
