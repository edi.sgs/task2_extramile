﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Task2API.Services;

namespace Task2API.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class CityController : ControllerBase
    {
        private readonly IDataService _dataService;
        public CityController(IDataService dataService)
        {
            _dataService = dataService;
        }

        // Endpoint : /city/list?code=CODE
        [HttpGet]
        public IEnumerable<City> List(string code)
        {
            return _dataService.GetCities(code);
        }

        // Endpoint : /city/query?code=CODE&query=QUERY
        [HttpGet]
        public IEnumerable<City> Query(string code, string search)
        {
            // If query item is null or empty then load all cities at given country
            if (string.IsNullOrEmpty(search))
            {
                return _dataService.GetCities(code);
            }

            return _dataService.QueryCities(code, search);
        }
    }
}
