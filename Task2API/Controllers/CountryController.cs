﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Task2API.Services;

namespace Task2API.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class CountryController : ControllerBase
    {

        private readonly IDataService _dataService;

        public CountryController(IDataService dataService)
        {
            _dataService = dataService;
        }

        //Endpoint : /country/list
        [HttpGet]
        public IEnumerable<Country> List()
        {
            return _dataService.GetCountries();
        }

        //Endpoint : /country/list?query=QUERY
        [HttpGet]
        public IEnumerable<Country> Query(string search)
        {
            if (string.IsNullOrEmpty(search))
            {
                return _dataService.GetCountries();
            }
            return _dataService.QueryCountries(search);
        }
    }
}
