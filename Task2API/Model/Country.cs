﻿namespace Task2API.Services
{
    public class Country
    {
        
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
