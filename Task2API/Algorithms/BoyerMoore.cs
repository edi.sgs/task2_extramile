﻿namespace Task2API.Algorithms
{
    class BoyerMoore
    {
        private readonly int[] _badCharShift;
        private string _pattern;

        public BoyerMoore(string pattern)
        {
            _pattern = pattern;
            _badCharShift = new int[256];

            // Preprocess the pattern
            ComputeBadCharShift();
        }

        private void ComputeBadCharShift()
        {
            int patternLength = _pattern.Length;
            for (int i = 0; i < 256; i++)
                _badCharShift[i] = patternLength;
            for (int i = 0; i < patternLength - 1; i++)
                _badCharShift[_pattern[i]] = patternLength - 1 - i;
        }

        public bool SearchInString(string text)
        {
            int patternLength = _pattern.Length;
            int textLength = text.Length;
            int shift = 0;

            while (shift <= textLength - patternLength)
            {
                int j = patternLength - 1;
                while (j >= 0 && _pattern[j] == text[shift + j])
                    j--;

                if (j < 0)
                {
                    return true; // Pattern found in the string
                }
                else
                {
                    shift += _badCharShift[text[shift + j]];
                }
            }

            return false; // Pattern not found in the string
        }
    }
}