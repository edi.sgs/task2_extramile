﻿namespace Task2API.Helpers
{
    public class ConverterHelper
    {
        public static float FahrenheitToCelsius(float fahrenheit)
        {
            float celsius = ((fahrenheit - 32) * 5.0f / 9.0f);
            return celsius;
        }
    }
}
