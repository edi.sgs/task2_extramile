﻿using System.Threading.Tasks;
using Task2API.Model;
using RestSharp;
using Newtonsoft.Json;
using System;
using Task2API.Helpers;

namespace Task2API.Services
{
    public class WeatherService : IWeatherService
    {
        private const string apiKey = "b0e5b64c10087d9196b58494b4b884f1";
        public WeatherService()
        {

        }

        // This method use to request weather data from openweathermap api service
        // Because Task 2 test reqirement to convert fahrenheit to celcius, then this method will fetch api service and add temp_celcius property value,
        // and then response back to the UI
        public async Task<WeatherModel> GetWeatherAsync(string query)
        {
            if (!ConnectionHelper.CheckForInternetConnection())
            {
                throw new Exception("No internet connection");
            }

            // Prepare api service request
            var resClient = new RestClient(baseUrl: "http://api.openweathermap.org");

            var req = new RestRequest("/data/2.5/weather", Method.Get);
            req.AddQueryParameter("q", query);

            // "appid" parameter means API KEY for openwhethermap api
            req.AddQueryParameter("appid", apiKey);

            try
            {
                var res = await resClient.ExecuteAsync(req);

                // For ignoring missing member e.g temp_celcius, etc
                var jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;

                // Parse json
                var weather = JsonConvert.DeserializeObject<WeatherModel>(res.Content, jsonSerializerSettings);

                // Add temp_celcius property value from temp (in fahrenheit)
                weather.main.temp_celcius = ConverterHelper.FahrenheitToCelsius(weather.main.temp);
                return weather;
            }
            catch (Exception)
            {

                throw ;
            }
        }

        // This method use for mock data
        public WeatherModel GetMockWeatherData()
        {
            var mockDataJson = @"
{
  'coord': {
    'lon': -0.1257,
    'lat': 51.5085
  },
  'weather': [
    {
      'id': 803,
      'main': 'Clouds',
      'description': 'broken clouds',
      'icon': '04d'
    }
  ],
  'base': 'stations',
  'main': {
    'temp': 285.8,
    'feels_like': 285.34,
    'temp_min': 284.18,
    'temp_max': 287.11,
    'pressure': 995,
    'humidity': 85
  },
  'visibility': 10000,
  'wind': {
    'speed': 2.06,
    'deg': 20
  },
  'clouds': {
    'all': 75
  },
  'dt': 1698236482,
  'sys': {
    'type': 2,
    'id': 2006068,
    'country': 'GB',
    'sunrise': 1698216048,
    'sunset': 1698252491
  },
  'timezone': 3600,
  'id': 2643743,
  'name': 'London',
  'cod': 200
}";

            // For ignoring missing member e.g temp_celcius, etc
            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;

            // Parse json
            var weather =  JsonConvert.DeserializeObject<WeatherModel> (mockDataJson, jsonSerializerSettings);

            // Add temp_celcius property value from temp (in fahrenheit)
            weather.main.temp_celcius = ConverterHelper.FahrenheitToCelsius(weather.main.temp);

            return weather;
        }
    }
}
