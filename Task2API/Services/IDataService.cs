﻿using System.Collections.Generic;

namespace Task2API.Services
{
    public interface IDataService
    {
        IEnumerable<Country> GetCountries();
        IEnumerable<Country> QueryCountries(string query);
        IEnumerable<City> GetCities(string countryCode);
        IEnumerable<City> QueryCities(string countryCode, string query);
    }
}
