﻿using System.Threading.Tasks;
using Task2API.Model;

namespace Task2API.Services
{
    public interface IWeatherService
    {
        Task<WeatherModel> GetWeatherAsync(string query);

        WeatherModel GetMockWeatherData();
    }
}
