﻿using System.Collections.Generic;
using System.Linq;
using Task2API.Algorithms;

namespace Task2API.Services
{
    public partial class DataService:IDataService
    {

        private List<City> _cities;
        private List<Country> _countries;

        public DataService()
        {
            // Mock data service
            // List of cities by given country code (only 2 cities for mock data/exmaple)
            _cities = new List<City>
            {
                new City { Name = "Berlin", CountryCode = "DE" },
                new City { Name = "Munich", CountryCode = "DE" },

                new City { Name = "Paris", CountryCode = "FR" },
                new City { Name = "Marseille", CountryCode = "FR" },

                new City { Name = "Rome", CountryCode = "IT" },
                new City { Name = "Milan", CountryCode = "IT" },

                new City { Name = "Madrid", CountryCode = "ES" },
                new City { Name = "Barcelona", CountryCode = "ES" },

                new City { Name = "London", CountryCode = "UK" },
                new City { Name = "Manchester", CountryCode = "UK" }

            };

            // List of countries, only 5 data for mock/example 
            _countries = new List<Country>
            {
                new Country { Name = "Germany", Code = "DE" },
                new Country { Name = "France", Code = "FR" },
                new Country { Name = "Italy", Code = "IT" },
                new Country { Name = "Spain", Code = "ES" },
                new Country { Name = "United Kingdom", Code = "UK" }
            };
        }

        //This method used for getting all the country
        public IEnumerable<Country> GetCountries()
        {
            //get all mock data from Country Class
            return _countries;
        }

        //  This method used to get countries with search query
        //  This is where Boyer Moore Algorithm used for effecient search query (usefull when data is above 500.000 records or so)
        //  This method is use when user typing Country name (in case like Select Box or Select2)
        public IEnumerable<Country> QueryCountries(string query) 
        {
            // Search using Boyer Moore Algorithm
            var boyerMoore = new BoyerMoore(query.ToLower().Trim());

            var result = new List<Country>();

            foreach (var country in _countries)
            {
                if (boyerMoore.SearchInString(country.Name.ToLower()))
                {
                    result.Add(country);
                }
            }

            //return the matching countries by query
            return result;
        }

        // This method used to get all cities by country code (case insensitive)
        public IEnumerable<City> GetCities(string countryCode)
        {
            if (countryCode == null || countryCode.Length == 0)
            {
                // if country code lenght is zero, return default or null data
                return default;
            }

            // Get City on given Country
            var resultCities = _cities.Where(c => c.CountryCode == countryCode.Trim().ToUpper());

            // Return the result
            return resultCities;
        }

        //This method used for query by cityname(case insensitive) on given country code
        public IEnumerable<City> QueryCities(string countryCode, string query)
        {
            if (countryCode == null || countryCode.Length == 0 || query == null || query.Length == 0)
            {
                // if country code or query lenght is zero or null, return default or null data
                return default;
            }

            // Search using Boyer Moore Algorithm
            var boyerMoore = new BoyerMoore(query.ToLower().Trim());

            var result = new List<City>();

            foreach (var country in _cities.Where(c => c.CountryCode == countryCode.ToUpper()))
            {
                if (boyerMoore.SearchInString(country.Name.ToLower()))
                {
                    result.Add(country);
                }
            }

            //return the matching countries by query
            return result;
        }
        
    }
}
