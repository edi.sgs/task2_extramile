using System.Linq;
using Task2API.Controllers;
using Task2API.Services;
using Xunit;

namespace Task2TEST.Tests
{
    public class CountryTest
    {
        private readonly IDataService _dataService;
        private readonly CountryController _countryController;


        // Constructor with dependency injection
        public CountryTest()
        {
            _dataService = new DataService();
            _countryController = new CountryController(_dataService);
        }

        [Fact]
        public void GetCountryTest()
        {
            var countries = _countryController.List();

            // Only 5 countries in array
            Assert.Equal(5, countries.Count());
        }

        // Test for query country, the query is "Ger" that means the result must be "Germany"
        [Theory]
        [InlineData("Ger")]
        public void QueryCountryTest(string query)
        {
            var countries = _countryController.Query(query);

            // Only 1 country in array with Germany name
            Assert.Single(countries);
            Assert.Equal("Germany", countries.First().Name);
        }
    }
}
