using System.Linq;
using Task2API.Controllers;
using Task2API.Services;
using Xunit;

namespace Task2TEST
{
    public class CityTest
    {
        private readonly IDataService _dataService;
        private readonly CityController _cityController;

        // Constructor with dependency injection
        public CityTest()
        {
            _dataService = new DataService();
            _cityController = new CityController(_dataService);
        }

        // Test get all city from Germany ("DE")
        [Theory]
        [InlineData("DE")]
        public void GetCityTest(string countryCode)
        {
            var cities = _cityController.List(countryCode);

            // Only 2 cities in variable with DE country code
            Assert.Equal(2, cities.Count());
        }

        [Theory]
        [InlineData("DE", "Ber")]
        public void QueryCityTest(string countryCode, string query)
        {
            var cities = _cityController.Query(countryCode, query);

            // The city with DE code and "Ber" query must be Berlin

            Assert.Single(cities);
            Assert.Equal("Berlin", cities.First().Name);
        }
    }
}
