﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;
using Task2API.Controllers;
using Task2API.Helpers;
using Task2API.Model;
using Task2API.Services;
using Xunit;

namespace Task2TEST.Tests
{
    public class WeatherTest
    {
        private readonly IWeatherService _weatherService;
        private readonly WeatherController _weatherController;

        public WeatherTest()
        {
            _weatherService = new WeatherService();
            _weatherController = new WeatherController(_weatherService);
        }

        

        // Service level test
        [Fact]
        public async void GetWeatherServiceTest()
        {
            // Test with london data

            WeatherModel weatherData = null;

            if (ConnectionHelper.CheckForInternetConnection())
            {
                // Load weather data from weather SERVICE
                weatherData = await _weatherService.GetWeatherAsync("London");
                
            }
            else
            {
                // If no internet connection just get from mock data
                weatherData = _weatherService.GetMockWeatherData();
            }

            Assert.NotNull(weatherData);

            Assert.Equal("London", weatherData.name);

            var tempCelcius = ConverterHelper.FahrenheitToCelsius(weatherData.main.temp);

            Assert.Equal(tempCelcius, weatherData.main.temp_celcius);
        }

        // Controller level test
        [Fact]
        public async void GetWeatherControllerTest()
        {
            // Test with london data

            WeatherModel weatherData;

            if (ConnectionHelper.CheckForInternetConnection())
            {
                // Load weather data from weather CONTROLLER
                weatherData = await _weatherController.Data("London");

            }
            else
            {
                // If no internet connection just get from mock data
                weatherData = _weatherService.GetMockWeatherData();
            }

            Assert.NotNull(weatherData);

            Assert.Equal("London", weatherData.name);

            var tempCelcius = ConverterHelper.FahrenheitToCelsius(weatherData.main.temp);

            Assert.Equal(tempCelcius, weatherData.main.temp_celcius);
        }
    }
}
